/***************************************************************/
/*                                                             */
/*   MIPS-32 Instruction Level Simulator                       */
/*                                                             */
/*   CS311 KAIST                                               */
/*   run.c                                                     */
/*                                                             */
/***************************************************************/

// Submitted by 20130201 Kim Hyunwoo & 20130405 Yu Jaemyung
#include <stdio.h>

#include "util.h"
#include "run.h"

int bp = 1;
int fwd = 0;
uint32_t TARGET;
int PCSRC, BR;
int INST_COUNT = 0;
struct ifid {
	int VALID;
	int STALL;
	instruction* Instr;
	uint32_t NPC;
} IF_ID;

struct idex {
	int VALID;
	int STALL;

	uint32_t NPC;
	int REG1;
	int REG2;
	short IMM;
	short OPCODE;
	char SHAMT;
	char FUNCT;
	char RS;
	char RT;

	char REG_DEST;
	char MEMWRITE;
	char MEMREAD;
	char MEMTOREG;
	char REGWRITE;
} ID_EX;

struct exmem {
	int VALID;

	int ALU_OUT;
	uint32_t BR_TARGET;
	uint32_t NPC;
	char REG_DEST;
	short OPCODE;
	int SWVALUE;

	char MEMWRITE;
	char MEMREAD;
	char MEMTOREG;
	char REGWRITE;
} EX_MEM;

struct memwb {
	int VALID;

	int ALU_OUT;
	int MEM_OUT;
	char REG_DEST;

	char MEMTOREG;
	char REGWRITE;

} MEM_WB;

/***************************************************************/
/*                                                             */
/* Procedure: get_inst_info                                    */
/*                                                             */
/* Purpose: Read insturction information                       */
/*                                                             */
/***************************************************************/
instruction* get_inst_info(uint32_t pc)
{
	return &INST_INFO[(pc - MEM_TEXT_START) >> 2];
}


/***************************************************************/
/*                                                             */
/* Procedure: process_instruction                              */
/*                                                             */
/* Purpose: Process one instrction                             */
/*                                                             */
/***************************************************************/
void process_instruction(){

    /* Your code */
	//WB Stage
	if (MEM_WB.VALID)
	{
		if (MEM_WB.REGWRITE) {  //Write to register
			if (MEM_WB.MEMTOREG)
				CURRENT_STATE.REGS[MEM_WB.REG_DEST] = MEM_WB.MEM_OUT;
			else
				CURRENT_STATE.REGS[MEM_WB.REG_DEST] = MEM_WB.ALU_OUT;
		}
		INST_COUNT++;
		CURRENT_STATE.PIPE[4] = CURRENT_STATE.PIPE[3];
	}
	else
		CURRENT_STATE.PIPE[4] = 0;
	
	//MEM_WB setting == MEM stage
	MEM_WB.VALID = EX_MEM.VALID;
	if (EX_MEM.VALID)
	{
		MEM_WB.REG_DEST = EX_MEM.REG_DEST;
		MEM_WB.MEMTOREG = EX_MEM.MEMTOREG;
		MEM_WB.REGWRITE = EX_MEM.REGWRITE;
		if (EX_MEM.MEMREAD)
			MEM_WB.MEM_OUT = mem_read_32(EX_MEM.ALU_OUT); //lw
		else if (EX_MEM.MEMWRITE)
			mem_write_32(EX_MEM.ALU_OUT, EX_MEM.SWVALUE); //sw
		else if ((EX_MEM.OPCODE == 4) || (EX_MEM.OPCODE == 5)) { //beq or bne
			if (bp == 1) { // branch prediction enabled
				if (IF_ID.NPC - 4 != EX_MEM.BR_TARGET) {
					IF_ID.VALID = 0;  //Flush ID stage
					ID_EX.VALID = 0;  //Flust EX stage
					TARGET = EX_MEM.BR_TARGET;
					PCSRC = 1; // Flush IF stage
				}
			}
			else {
				IF_ID.VALID = 0;  //Flush ID stage
				ID_EX.VALID = 0;  //Flust EX stage
				TARGET = EX_MEM.BR_TARGET;
				PCSRC = 1; // Flush IF stage
				BR = 1;
			}
		}
		else
			MEM_WB.ALU_OUT = EX_MEM.ALU_OUT;
		CURRENT_STATE.PIPE[3] = CURRENT_STATE.PIPE[2];
	}
	else {
		CURRENT_STATE.PIPE[3] = 0;
	}
	
	//EX_MEM setting == EX stage
	EX_MEM.VALID = ID_EX.VALID;
	if (ID_EX.VALID)
	{
		EX_MEM.NPC = ID_EX.NPC;
		EX_MEM.REG_DEST = ID_EX.REG_DEST;
		EX_MEM.OPCODE = ID_EX.OPCODE;
		EX_MEM.MEMWRITE = ID_EX.MEMWRITE;
		EX_MEM.MEMREAD = ID_EX.MEMREAD;
		EX_MEM.MEMTOREG = ID_EX.MEMTOREG;
		EX_MEM.REGWRITE = ID_EX.REGWRITE;
		switch (ID_EX.OPCODE) {
		case 0: // R type
			switch (ID_EX.FUNCT) {
			case 33: //addu
				EX_MEM.ALU_OUT = ID_EX.REG1 + ID_EX.REG2;
				break;

			case 35: //subu
				EX_MEM.ALU_OUT = ID_EX.REG1 - ID_EX.REG2;
				break;

			case 36: //and
				EX_MEM.ALU_OUT = ID_EX.REG1 & ID_EX.REG2;
				break;

			case 37: //or
				EX_MEM.ALU_OUT = ID_EX.REG1 | ID_EX.REG2;
				break;

			case 39: //nor
				EX_MEM.ALU_OUT = ~(ID_EX.REG1 | ID_EX.REG2);
				break;

			case 8: //jr
				break;

			case 0: //sll
				EX_MEM.ALU_OUT = (ID_EX.REG2 << ID_EX.SHAMT);
				break;
			case 2: //srl
				EX_MEM.ALU_OUT = (ID_EX.REG2 >> ID_EX.SHAMT) & (~(((1 << 31) >> ID_EX.SHAMT) << 1));
				break;

			case 43: //sltu
				EX_MEM.ALU_OUT = (ID_EX.REG1 < ID_EX.REG2) ? 1 : 0;
				break;
			default:
				//printf("Unsupported R type instruction %x at %x \n", currentIns.value, CURRENT_STATE.PC);
				RUN_BIT = FALSE;
			}

			break;
		case 3: // jal
			EX_MEM.ALU_OUT = ID_EX.NPC;
			break;
		case 2: // j	
			break;

		case 4: //beq
			if (ID_EX.REG1 == ID_EX.REG2)
				EX_MEM.BR_TARGET = ID_EX.NPC + (SIGN_EX(ID_EX.IMM) * 4);
			else
				EX_MEM.BR_TARGET = ID_EX.NPC;
			IF_ID.VALID = 0;
			if (!bp) {
				IF_ID.STALL = 1;
				TARGET = CURRENT_STATE.PC;
				PCSRC = 1;
				BR = 1;
			}
			break;

		case 5: //bne
			if (ID_EX.REG1 != ID_EX.REG2)
				EX_MEM.BR_TARGET = ID_EX.NPC + (SIGN_EX(ID_EX.IMM) * 4);
			else
				EX_MEM.BR_TARGET = ID_EX.NPC;
			IF_ID.VALID = 0;
			if (!bp) {
				IF_ID.STALL = 1;
				TARGET = CURRENT_STATE.PC;
				PCSRC = 1;
				BR = 1;
			}
			break;

		case 9: //addiu
			EX_MEM.ALU_OUT = ID_EX.REG1 + SIGN_EX(ID_EX.IMM);
			break;

		case 11: // sltiu
			EX_MEM.ALU_OUT = (ID_EX.REG1 < ID_EX.IMM) ? 1 : 0;
			break;

		case 12: //andi
			EX_MEM.ALU_OUT = (ID_EX.REG1 & ID_EX.IMM);
			break;

		case 13: //ori
			EX_MEM.ALU_OUT = (ID_EX.REG1 | ID_EX.IMM);
			break;

		case 15: //lui
			EX_MEM.ALU_OUT = (ID_EX.IMM << 16);
			break;

		case 35: //lw
			EX_MEM.ALU_OUT = (ID_EX.REG1 + SIGN_EX(ID_EX.IMM));
			break;

		case 43: //sw
			EX_MEM.ALU_OUT = (ID_EX.REG1 + SIGN_EX(ID_EX.IMM));
			EX_MEM.SWVALUE = ID_EX.REG2;

			break;

		default:
			//printf("Unsupported instruction %x at %x \n", currentIns.value, CURRENT_STATE.PC);
			RUN_BIT = FALSE;
			break;
		}
		CURRENT_STATE.PIPE[2] = CURRENT_STATE.PIPE[1];
	}
	else
		CURRENT_STATE.PIPE[2] = 0;
	
	

	
	//ID_EX setting == ID stage
	ID_EX.VALID = IF_ID.VALID;
	if (IF_ID.VALID && !ID_EX.STALL) {
		IF_ID.STALL = 0;
		ID_EX.NPC = IF_ID.NPC;
		ID_EX.RS = RS(IF_ID.Instr);
		ID_EX.RT = RT(IF_ID.Instr);
		ID_EX.REG1 = CURRENT_STATE.REGS[RS(IF_ID.Instr)];
		ID_EX.REG2 = CURRENT_STATE.REGS[RT(IF_ID.Instr)];
		ID_EX.IMM = IMM(IF_ID.Instr);
		ID_EX.OPCODE = OPCODE(IF_ID.Instr);
		ID_EX.SHAMT = SHAMT(IF_ID.Instr);
		ID_EX.FUNCT = FUNC(IF_ID.Instr);
		ID_EX.REG_DEST = (ID_EX.OPCODE == 0) ? RD(IF_ID.Instr) : RT(IF_ID.Instr);
		ID_EX.MEMWRITE = 0;
		ID_EX.MEMREAD = 0;
		ID_EX.MEMTOREG = 0;
		ID_EX.REGWRITE = 1;
		PCSRC = 0;
		switch (ID_EX.OPCODE) {
		case 0:
			if (ID_EX.FUNCT == 8) { // jr
				ID_EX.REGWRITE = 0;
				TARGET = ID_EX.REG1;
				PCSRC = 1;
			}
			break;
		case 35: // lw
			ID_EX.MEMREAD = 1;
			ID_EX.MEMTOREG = 1;
			break;
		case 43: // sw
			ID_EX.MEMWRITE = 1;
			ID_EX.REGWRITE = 0;
			break;
		case 3: //jal
			ID_EX.REG_DEST = 31;
			TARGET = ((IF_ID.NPC & 0xF0000000) | (TARGET(IF_ID.Instr) << 2));
			PCSRC = 1;
			//ID_EX.STALL = 0;
			break;
		case 2: //j
			TARGET = ((IF_ID.NPC & 0xF0000000) | (TARGET(IF_ID.Instr) << 2));
			PCSRC = 1;
			ID_EX.REGWRITE = 0;
			//PCSTALL = 0;
			break;
		case 4: //beq and bne
		case 5: //branch prediction enabled here
			if (bp) {
				TARGET = IF_ID.NPC + (SIGN_EX(ID_EX.IMM) * 4);
				PCSRC = 1;
				//IF_ID.STALL = 1;
			}
			else {
				BR = 1;
			}
			ID_EX.REGWRITE = 0;
			break;
		}
		CURRENT_STATE.PIPE[1] = CURRENT_STATE.PIPE[0];
		if (fwd){
			
			ID_EX.STALL = ((RS(IF_ID.Instr) != 0) || (RT(IF_ID.Instr) != 0))
				&& (EX_MEM.REGWRITE && EX_MEM.MEMTOREG && (EX_MEM.VALID && ((RS(IF_ID.Instr) == EX_MEM.REG_DEST) 
					|| ((OPCODE(IF_ID.Instr) == 0) && (RT(IF_ID.Instr) == EX_MEM.REG_DEST)))));
			if (!ID_EX.STALL) {
				if ((RS(IF_ID.Instr) != 0) &&
					(MEM_WB.VALID && ((RS(IF_ID.Instr) == MEM_WB.REG_DEST)) && MEM_WB.REGWRITE)) {
					ID_EX.REG1 = MEM_WB.MEMTOREG ? MEM_WB.MEM_OUT : MEM_WB.ALU_OUT;
				}
				if ((RS(IF_ID.Instr) != 0) 
					&& (EX_MEM.REGWRITE && !EX_MEM.MEMTOREG && (EX_MEM.VALID && ((RS(IF_ID.Instr) == EX_MEM.REG_DEST))))) {
					ID_EX.REG1 = EX_MEM.ALU_OUT;
				}
				if ((RT(IF_ID.Instr) != 0) &&
					((OPCODE(IF_ID.Instr) == 0) && (RT(IF_ID.Instr) == MEM_WB.REG_DEST) && MEM_WB.REGWRITE)) {
					ID_EX.REG2 = MEM_WB.MEMTOREG ? MEM_WB.MEM_OUT : MEM_WB.ALU_OUT;
				}
				if ((RT(IF_ID.Instr) != 0) &&
					(EX_MEM.REGWRITE && !EX_MEM.MEMTOREG && (EX_MEM.VALID && ((OPCODE(IF_ID.Instr) == 0) && (RT(IF_ID.Instr) == EX_MEM.REG_DEST))))) {
					ID_EX.REG2 = EX_MEM.ALU_OUT;
				}

			}
		}
		else {
			ID_EX.STALL = ((RS(IF_ID.Instr) != 0) || (RT(IF_ID.Instr) != 0))
				&& ((EX_MEM.VALID && ((RS(IF_ID.Instr) == EX_MEM.REG_DEST) || ((OPCODE(IF_ID.Instr) == 0) && (RT(IF_ID.Instr) == EX_MEM.REG_DEST))))
					|| (MEM_WB.VALID && ((RS(IF_ID.Instr) == MEM_WB.REG_DEST) || ((OPCODE(IF_ID.Instr) == 0) && (RT(IF_ID.Instr) == MEM_WB.REG_DEST)))));
		}
		if (ID_EX.STALL) {
			ID_EX.VALID = 0;
		}
	}
	else if (ID_EX.STALL) { //pipeline stalled
		if (fwd) {
			ID_EX.STALL = ((ID_EX.RS != 0) || (ID_EX.RT != 0))
				&& (EX_MEM.REGWRITE && EX_MEM.MEMTOREG && (EX_MEM.VALID && (ID_EX.RS == EX_MEM.REG_DEST)
					|| ((ID_EX.OPCODE == 0) && (ID_EX.RT == EX_MEM.REG_DEST))));
		}
		else {
			ID_EX.STALL = ((ID_EX.RS != 0) || (ID_EX.RT != 0))
				&& ((EX_MEM.VALID && ((ID_EX.RS == EX_MEM.REG_DEST) || ((ID_EX.OPCODE == 0) && (ID_EX.RT == EX_MEM.REG_DEST))))
					|| (MEM_WB.VALID && ((ID_EX.RS == MEM_WB.REG_DEST) || ((ID_EX.OPCODE == 0) && (ID_EX.RT == MEM_WB.REG_DEST)))));
		}

		if (ID_EX.STALL) {
			//IF_ID.STALL = ID_EX.STALL;
			ID_EX.VALID = 0;
		}
		else {
			ID_EX.REG1 = CURRENT_STATE.REGS[ID_EX.RS];
			ID_EX.REG2 = CURRENT_STATE.REGS[ID_EX.RT];

			if (fwd) {
				if ((ID_EX.RS != 0) &&
					(MEM_WB.VALID && ((ID_EX.RS == MEM_WB.REG_DEST)) && MEM_WB.REGWRITE)) {
					ID_EX.REG1 = MEM_WB.MEMTOREG ? MEM_WB.MEM_OUT : MEM_WB.ALU_OUT;
				}
				if ((ID_EX.RS != 0)
					&& (EX_MEM.REGWRITE && !EX_MEM.MEMTOREG && (EX_MEM.VALID && ((ID_EX.RS == EX_MEM.REG_DEST))))) {
					ID_EX.REG1 = EX_MEM.ALU_OUT;
				}
				if ((ID_EX.RT != 0) &&
					((ID_EX.OPCODE == 0) && (ID_EX.RT == MEM_WB.REG_DEST) && MEM_WB.REGWRITE)) {
					ID_EX.REG2 = MEM_WB.MEMTOREG ? MEM_WB.MEM_OUT : MEM_WB.ALU_OUT;
				}
				if ((ID_EX.RT != 0) &&
					(EX_MEM.REGWRITE && !EX_MEM.MEMTOREG && (EX_MEM.VALID && ((ID_EX.OPCODE == 0) && (ID_EX.RT == EX_MEM.REG_DEST))))) {
					ID_EX.REG2 = EX_MEM.ALU_OUT;
				}
			}
		}
		//CURRENT_STATE.PIPE[2] = 0;

	}
	else { //IF flushed
		CURRENT_STATE.PIPE[1] = bp ? (PCSRC ? CURRENT_STATE.PIPE[0] : 0) : 0;


	}
	

	//IF_ID setting == IF stage
	IF_ID.VALID = !PCSRC;
	
	if (!PCSRC) {
		if (!IF_ID.STALL) {
			IF_ID.Instr = get_inst_info(CURRENT_STATE.PC);
			IF_ID.NPC = CURRENT_STATE.PC + 4;

			if (CURRENT_STATE.PC < (MEM_REGIONS[0].start + (NUM_INST * 4))) {
				CURRENT_STATE.PIPE[0] = CURRENT_STATE.PC;
				CURRENT_STATE.PC += 4;
			}
			else {
				CURRENT_STATE.PIPE[0] = 0;
				IF_ID.VALID = 0;
			}

		}
	}
	else {
		if (bp) {
			CURRENT_STATE.PIPE[0] = CURRENT_STATE.PC;
			CURRENT_STATE.PC = TARGET;
			PCSRC = 0;
		}
		else {
			if (IF_ID.STALL) {
				CURRENT_STATE.PIPE[0] = 0;
				CURRENT_STATE.PC = TARGET;
			}
			else {
				CURRENT_STATE.PIPE[0] = BR? 0: CURRENT_STATE.PC;
				CURRENT_STATE.PC = TARGET;
				PCSRC = 0;
			}
		}
	}

	BR = 0;
	IF_ID.STALL = ID_EX.STALL;

		
	if (CURRENT_STATE.PC < MEM_REGIONS[0].start || CURRENT_STATE.PC >= (MEM_REGIONS[0].start + (NUM_INST * 4)))
		//if ((CURRENT_STATE.PIPE[3] == 0) && (CURRENT_STATE.PIPE[2] == 0) && (CURRENT_STATE.PIPE[1] == 0) && (CURRENT_STATE.PIPE[0] == 0))
			RUN_BIT = FALSE;
	return;
}
